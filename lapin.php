<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Accueil</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container ">
      <a class="navbar-brand" href="index.html"><img src="/img/logo SV.jpg" alt="" width="30" height="24"></a>
      <a class="navbar-brand" href="index.html">Bienvenu sur Animara Matcher</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto mb-2 mb-lg-0 ">
          <li class="nav-item"><a class="nav-link active" aria-current="page" href="chiens.html">Chiens</a></li>
          <li class="nav-item"><a class="nav-link active" aria-current="page" href="index.html">Chats</a></li>
          <li class="nav-item"><a class="nav-link active" aria-current="page" href="lapin.html">Lapins</a></li>
          <li class="nav-item"><a class="nav-link active" aria-current="page" href="cochon dinde.html">Cochon d'inde</a></li>
          <a class="nav-link" href="#">Profil</a>
          </li>
        </ul>
        <form class="d-flex">
          <input class="form-control mx-2" type="search" placeholder="Rechercher" aria-label="Search">
          <button class="btn btn-success" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
              fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
              <path
                d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
            </svg></button>
        </form>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row mt-5">
      <div class="col-12">
        <h1 class="text-center">Animara Matcher</h1>
      </div>
    </div>
  </div>

  <img class="banner mt-3" src="img/banner-rabbit.png">

  <div class="container">
    <div class="row mt-5">
      <div class="col-12">
        <h4>Concept</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla consequuntur, doloribus praesentium
          dignissimos dicta quas sint sed ducimus magnam laudantium temporibus, libero error ullam qui minus reiciendis
          voluptates facilis maiores. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod quidem facilis
          tempora fuga exercitationem dicta quam corporis quasi. Quo asperiores enim nam laboriosam cupiditate
          repudiandae harum nesciunt reprehenderit nihil ullam.</p>
        <h2 class="text-center mt-5 mb-5">Catégories</h2>
        <div class="row">
          <div class="col-12 col-lg-3">
            <div class="text-center">
              <img class="img-bloc" src="img/1.png">
              <button type="button" class="btn btn-primary btn-sm mt-1" style="text-align: center;">Consulter</button>
            </div>
          </div>
          <div class="col-12 col-lg-3">
            <div class="text-center">
            <img class="img-bloc" src="img/4.png">
            <button type="button" class="btn btn-primary btn-sm mt-1" style="text-align: center;">Consulter</button>
          </div>
        </div>
          <div class="col-12 col-lg-3">
            <div class="text-center">
            <img class="img-bloc" src="img/2.png">
            <button type="button" class="btn btn-primary btn-sm mt-1" style="text-align: center;">Consulter</button>
          </div>
        </div>
          <div class="col-12 col-lg-3">
            <div class="text-center">
            <img class="img-bloc" src="img/9.png">
            <button type="button" class="btn btn-primary btn-sm mt-1" style="text-align: center;">Consulter</button>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>



</body>

</html>