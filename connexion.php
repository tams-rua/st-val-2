<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<?php
//connexion
$dbname = 'animara_matcher';
$username = 'root';
$password = '';

$error = false;

try{
    $bdd = new PDO('mysql:host=localhost;dbname=animara_matcher; port=3306', $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}  catch (PDOException $e) {
    echo 'Échec lors de la connexion : ' . $e->getMessage();
}


if(isset($_POST['login'])) {
    $email = $_POST["email"];
    $password = $_POST["password"];

    $query = "SELECT * FROM users WHERE user_mail LIKE '" . $email . "' AND user_password LIKE '" . $password . "'";

    $sth = $bdd->prepare($query);
    $sth->execute();
    $result = $sth->fetch(PDO::FETCH_ASSOC);
    if($result) header('Location: index.php');
    else $error = true;
    
}
?>
<body>

    <div class="container">
        <div class="card col-12 col-6 mt-5 mx-auto bg-dark text-light" style="width: 30rem;">
            <form method="POST" id="signup-form" class="signup-form text-center" action="login.php">
                <h2 class="form-title m-4 text-uppercase">Se connecter</h2>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" id="email" placeholder="email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                </div>
                <input type="hidden" name="login" value="1">
                <p class="loginhere text-center">
                    Déja un compte ? <a href="#" class="loginhere-link">se connecter</a>
                </p>
                <button type="submit" class="btn btn-success m-2">Enregistrer</button>
            </form>
            <?php if($error){ ?>
            <p>Erreur sur la connexion</p>
            <?php } ?>
        </div>
    </div>

    
    
</body>
</html>